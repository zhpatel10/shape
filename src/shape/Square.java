/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

public class Square extends Shape {
    
     @Override
    public double getArea(double side)
    {
    return side*side;
    }
    
    @Override
    public double getPerimeter(double side)
    {
    return 4*side;
    }
}
