/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

public class Test {
    public static void main(String[] args) {
        Circle c1=new Circle();
        System.out.println("Area of circle with radius 8: "+ c1.getArea(8));
        System.out.println("Perimeter of circle with radius 8: "+ c1.getPerimeter(8));
        
        Square sq1=new Square();
        System.out.println("Area of square with side 6: "+ sq1.getArea(6));
        System.out.println("Perimeter of square with side 6: "+ sq1.getPerimeter(6));
        
    }
}
