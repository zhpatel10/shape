/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    package shape;

    public class Circle extends Shape {

        @Override
        public double getArea(double radius)
        {
        return Math.PI*radius*radius;
        }

        @Override
        public double getPerimeter(double radius)
        {
        return 2*Math.PI*radius;
        }
    }
